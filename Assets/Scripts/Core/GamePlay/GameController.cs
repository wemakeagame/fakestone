﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    public PlayerController player1;
    public PlayerController player2;

    public int totalMana;
    public bool isPlayerOneStarting;
    public int currentTurn = 1;

    public static GameController instance;

    // Use this for initialization
    void Start()
    {
        instance = this;
        int rand = Random.Range(1, 100);
        isPlayerOneStarting = rand > 50;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ExitGame()
    {
        SceneManager.LoadScene("Menu");
    }
}
