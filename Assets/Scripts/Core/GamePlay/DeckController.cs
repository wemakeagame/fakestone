﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckController : MonoBehaviour
{

    private Vector3 initialSize;
    private int totalInitialCards;
    public List<CardBase> initListCard;
    private PlayerController player;

    //animation variables
    public float timeToShowPlayer;
    public float dumbGetCard;
    private float currentTimeToShowPlayer;
    private bool moveToHand;
    private Vector3 positionShowPlayer;
    private Vector3 positionHand;
    private Vector3 targetPosition;

    private GameObject tempCard;
    private bool canPlayerControl;


    // Use this for initialization
    void Start()
    {
        initialSize = transform.localScale;
        totalInitialCards = initListCard.Count;
        positionShowPlayer = player.hand.positionToShowPlayer.position;
        canPlayerControl = player.canPlayerControl;
    }

    // Update is called once per frame
    void Update()
    {
        if (moveToHand && tempCard != null)
        {
            currentTimeToShowPlayer += Time.deltaTime;
            if (currentTimeToShowPlayer > timeToShowPlayer)
            {
                positionHand = player.hand.positionNextCard;
                targetPosition = positionHand;
            }

            tempCard.transform.position = Vector3.Lerp(tempCard.transform.position, targetPosition, dumbGetCard * Time.deltaTime);

            if (Vector3.Distance(tempCard.transform.position, positionHand) < 2)
            {
                CardBase tempCardComponent = tempCard.GetComponent<CardBase>();
                tempCardComponent.SetOnHand();
                tempCardComponent.SetStartPosition(positionHand);
                tempCard = null;
            }

        }

    }

    public void GetCard()
    {
        if (initListCard.Count > 0)
        {
            int randCardIndex = Random.Range(0, initListCard.Count);
            CardBase selectedCard = initListCard[randCardIndex];
            initListCard.RemoveAt(randCardIndex);

            tempCard = Instantiate(selectedCard.gameObject, transform.position, selectedCard.transform.rotation) as GameObject;
            ResizeDeck();
            moveToHand = true;
            targetPosition = positionShowPlayer;
            currentTimeToShowPlayer = 0;
            player.hand.AddCard(tempCard.GetComponent<CardBase>(), canPlayerControl);

        }
    }

    private void ResizeDeck()
    {
        Vector3 newSize = transform.localScale;

        newSize.y = initListCard.Count * initialSize.y / totalInitialCards;
        transform.localScale = newSize;

        if (initListCard.Count == 0)
        {
            GetComponent<Renderer>().enabled = false;
        }


    }

    public void SetupDeck(PlayerController playerToSet)
    {
        player = playerToSet;
    }

}
