﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardController : MonoBehaviour
{

    public StoneBehaviour stonePrefab;
    public List<StoneBehaviour> stones;
    public Transform minPosition;
    public Transform maxPosition;
    public Vector3 positionNextStone;

    // Use this for initialization
    void Start()
    {
        ReorganizeBoard();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddStone(CardBase card)
    {
        GameObject tempStone = Instantiate(stonePrefab.gameObject) as GameObject;
        CardMinion selectedCard = card.GetComponent<CardMinion>();
        tempStone.transform.position = positionNextStone;
        tempStone.GetComponent<StoneBehaviour>().SetStone(
            selectedCard.attackPower,
            selectedCard.GetComponent<LifeBase>().GetCurrentLife(),
            selectedCard.imgCard,
            true);

        stones.Add(tempStone.GetComponent<StoneBehaviour>());

        ReorganizeBoard();
    }

    public void ReorganizeBoard()
    {
        Vector3 position = (minPosition.position + maxPosition.position) / 2;

        for (int i = 1; i < stones.Count+1; i++)
        {
            position = CalcDistanceStonePosition(i, stones.Count + 1);
            if (i - 1 < stones.Count)
            {

                Debug.Log(position);
                //stones[i - 1].SetStartPosition(position);
                stones[i - 1].transform.position = position;
            }

        }

        positionNextStone = CalcDistanceStonePosition(stones.Count, stones.Count + 1);
    }

    private Vector3 CalcDistanceStonePosition(int indice, int limit)
    {
        float distance = indice / (float)limit;

        return Vector3.Lerp(minPosition.position, maxPosition.position, distance);
    }
}
