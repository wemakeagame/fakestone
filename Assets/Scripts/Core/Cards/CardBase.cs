﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardBase : MonoBehaviour
{
    public int totalManaRequest;
    public string nameCard;
    public string descriptionCard;
    public Texture imgCard;


    public TextMesh textMana;
    public TextMesh textNameCard;
    public TextMesh textDescritionCard;
    public Renderer renderImgCard;

    public float dumbDragMovimentation;
    public Vector3 offsetZoomPosition;

    private Camera mainCamera, perspectiveCamera;
    private Vector3 startPosition, zoomPosition;
    private Vector3 positionToGo;
    private bool onHand, canPlayerControl, isFaceShowing = true;
    private bool isDragging;


    // Use this for initialization
    protected void Start()
    {
        textMana.text = totalManaRequest.ToString();
        textNameCard.text = nameCard;
        textDescritionCard.text = descriptionCard;
        renderImgCard.material.mainTexture = imgCard;
        mainCamera = Camera.main;
        perspectiveCamera = Camera.allCameras[0];
        GetComponentInChildren<Canvas>().worldCamera = perspectiveCamera;

    }

    // Update is called once per frame
    protected void Update()
    {
        if (transform.position != positionToGo && onHand)
        {
            transform.position = Vector3.Lerp(transform.position, positionToGo, Time.deltaTime * dumbDragMovimentation);
        }
    }

    public void OnClick()
    {

    }

    public void OnMouseHover()
    {
        if (onHand && !isDragging)
        {
            positionToGo = zoomPosition;
        }
    }

    public void OnMouseExit()
    {
        if (onHand && !isDragging)
        {
            positionToGo = startPosition;
        }
    }

    public void OnDrag()
    {
        if (onHand)
        {
            isDragging = true;

            positionToGo = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            positionToGo.y = startPosition.y;
        }
    }

    public void OnDrop()
    {
        if (onHand)
        {
            isDragging = false;
            positionToGo = startPosition;
            ToggleLayer();
        }
    }

    public void OnStartDrag()
    {
        if (onHand)
        {
            ToggleLayer();
        }
    }

    public void FlipCard()
    {
        isFaceShowing = !isFaceShowing;
        Vector3 newRotation = transform.eulerAngles;
        if (isFaceShowing)
        {
            newRotation.z = 0;
        }
        else
        {
            newRotation.z = 180;
        }
        transform.eulerAngles = newRotation;
    }

    public void SetOnHand()
    {
        onHand = true;
        
        
    }
    public void SetOwner(bool canPlayerControl)
    {
        this.canPlayerControl = canPlayerControl;
        if (!canPlayerControl)
        {
            FlipCard();
        }
    }

    public void SetStartPosition(Vector3 position)
    {
        startPosition = position;
        positionToGo = startPosition;
        zoomPosition = startPosition + offsetZoomPosition;
    }

    public void ToggleLayer()
    {
        int newLayer;
        if (gameObject.layer == LayerMask.NameToLayer("UI"))
        {
            newLayer = LayerMask.NameToLayer("Default");
        }
        else
        {
            newLayer = LayerMask.NameToLayer("UI");
        }

        Transform[] transformsCard = GetComponentsInChildren<Transform>();
        foreach (Transform t in transformsCard)
        {
            t.gameObject.layer = newLayer;
        }
    }






}
