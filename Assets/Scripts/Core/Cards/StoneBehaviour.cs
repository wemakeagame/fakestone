﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneBehaviour : MonoBehaviour
{
    public Renderer meshShield, meshStone;
    public TextMesh lifeText, attackText;

    private bool isTaunt = false;
    private int life, attack;
    private Vector3 startPostion;

    // Use this for initialization
    void Awake()
    {
        meshShield.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetStartPosition (Vector3 position)
    {
        startPostion = position;
        transform.position = startPostion;
    }

    public void SetStone(int attack, int life, Texture imgCard, bool isTaunt)
    {
        this.attack = attack;
        this.life = life;
        this.isTaunt = isTaunt;
        meshStone.materials[1].mainTexture = imgCard;
        ApplyTaunt();
        UpdateValues();
    }

    private void UpdateValues()
    {
        lifeText.text = life.ToString();
        attackText.text = attack.ToString();
    }

    public void ApplyTaunt()
    {
        meshShield.enabled = isTaunt;
                
    }

}
