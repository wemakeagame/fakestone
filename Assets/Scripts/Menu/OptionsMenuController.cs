﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenuController : MonoBehaviour {

    public Toggle toggleSoundSFX;
    public Toggle toggleSoundMusic;
    public Slider sliderVolumeSFX;
    public Slider sliderVolumeMusic;

    // Use this for initialization
    void Start ()
    {
        if (ApplicationControler.isFirstTime())
        {
            ApplicationControler.SetDefaultConfigs();
        }
        toggleSoundSFX.isOn = ApplicationControler.IsMutedSoundSFX();
        toggleSoundMusic.isOn = ApplicationControler.IsMutedSoundMusic();
        sliderVolumeSFX.value = ApplicationControler.GetVolumeSFX();
        sliderVolumeMusic.value = ApplicationControler.GetVolumeMusic();
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void SetSFXSound( )
    {
        if (toggleSoundSFX.isOn )
        {
            ApplicationControler.EnableSoundSFX();
        }
        else
        {
            ApplicationControler.DisableSoundSFX();
        }
    }

    public void SetMusicSound()
    {
        if (toggleSoundMusic.isOn)
        {
            ApplicationControler.EnableSoundMusic();
        }
        else
        {
            ApplicationControler.DisableSoundMusic();
        }
    }

    public void SetVolumeSFX()
    {
        ApplicationControler.SetVolumeSFX(sliderVolumeSFX.value);
    }

    public void SetVolumeMusic()
    {
        ApplicationControler.SetVolumeMusic(sliderVolumeMusic.value);
    }

}
