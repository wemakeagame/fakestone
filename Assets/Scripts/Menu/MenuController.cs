﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    public GameObject generalMenu;
    public GameObject optionMenu;



	// Use this for initialization
	void Start ()
    {        
        ActiveMenu(generalMenu);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void HideMenu()
    {
        optionMenu.SetActive(false);
        generalMenu.SetActive(false);
    } 

    public void ActiveMenu(GameObject menu)
    {
        HideMenu();
        menu.SetActive(true);
    }

    public void ExitGame()
    {
        ApplicationControler.ExitGame();
    }

    public void Play()
    {
        SceneManager.LoadScene("GamePlay");
    }

    
}
